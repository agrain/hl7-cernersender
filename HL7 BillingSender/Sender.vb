﻿Imports System.Net.Sockets
Public Class Sender
    Public Shared Function socketSendReceive(ByRef client As TcpClient, ByVal szSendData As String) As String
        Dim szResult As String = ""
        Dim bytes As Integer
        'Set up variables and String to write to the server.
        Dim msg() As Byte
        Dim RecvBytes(8191) As Byte
        Dim stream As NetworkStream
        Try
            stream = client.GetStream
            msg = System.Text.Encoding.ASCII.GetBytes(Chr(11) & szSendData & Chr(28) & Chr(13))
            stream.Write(msg, 0, msg.Length)
        Catch ex As SocketException
            Return ""
        Catch ex1 As Exception
            Return ""
        End Try
        szResult = ""
        Dim bCompleteMessage As Boolean = False
        Do
            ' Receive the server response
            Try
                bytes = stream.Read(RecvBytes, 0, RecvBytes.Length)
            Catch ex As SocketException
                Return ""
            Catch ex1 As Exception
                Return ""
            End Try
            If bytes = 0 Then Return ""
            szResult = szResult & System.Text.Encoding.ASCII.GetString(RecvBytes, 0, bytes)
            If Mid(szResult, 1, 1) = Chr(11) Then
                szResult = Mid(szResult, 2)
            End If
            If Microsoft.VisualBasic.Right(szResult, 2) = Chr(28) & Chr(13) Then
                szResult = Left(szResult, Len(szResult) - 2)
                bCompleteMessage = True
            End If
        Loop While Not bCompleteMessage
        Return szResult
    End Function 'socketSendReceive
End Class
