﻿Imports System.Text
Imports HL7_CernerSender.Logger
Imports System.Threading
Public Class HL7
    Public MCID As String = String.Empty
    Private _MSG As String = String.Empty
    Public Property MSG() As String
        Get
            Return _MSG
        End Get
        Set(ByVal value As String)
            _MSG = value
        End Set
    End Property

    Public Sub New(ByVal r As HL7_CernerSender.Cerner_Interface, ByVal sf As String)
        'Dim ReportType As String = "R"
        'MCID = ReadStep(sf, Logger.stepType.count)
        Dim bTemplate As New StringBuilder
        'MCID += 1
        'MSH
        bTemplate.Append("MSH|^~\&|AH_GENI|1031|HSIE|1031|")
        bTemplate.Append(Format(Now, "yyyyMMddHHmmss"))
        bTemplate.Append("||ORU^R01|DA")
        'bTemplate.Append(MCID)
        bTemplate.Append(Format(Now, "mmss"))
        Dim rr As Random = New Random(Format(Now, "hhmmssfff"))
        Dim ss As String = Math.Round(rr.NextDouble, 8).ToString
        bTemplate.Append(ss.Substring(2))
        bTemplate.Append(Format(Now, "hhmmss"))
        bTemplate.Append("|T|2.4")
        bTemplate.Append(Chr(13))

        'WriteStep(sf, Logger.stepType.count, MCID)

        'append HL7 Template to string
        bTemplate.Append("PID|||^^^^||^|||" + Chr(13))
        bTemplate.Append("PV1||" + Chr(13))
        bTemplate.Append("ORC|||^|" + Chr(13))
        bTemplate.Append("OBR|||^|^|||||||||||||||||||||||^^^" + Chr(13))
        bTemplate.Append("OBX|||^^|||||||||||||||" + Chr(13))

        MSG = bTemplate.ToString

        InsertItem("PID_3_1_ID", r.PID_3_1_ID)
        InsertItem("PID_3_4_Assigning_Authority", r.PID_3_4_Assigning_Authority)
        InsertItem("PID_3_5_Identifier_Type_Code", r.PID_3_5_Identifier_Type_Code)
        InsertItem("PID_5_1_Family_Name", r.PID_5_1_Family_Name)
        InsertItem("PID_5_2_Given_Name", r.PID_5_2_Given_Name)
        InsertItem("PID_7_Date_Time_of_Birth", r.PID_7_Date_Time_of_Birth)
        InsertItem("PID_8_Sex", r.PID_8_Sex)

        InsertItem("PV1_1_Visit_Number", "1")
        InsertItem("PV1_2_Visit_Number", "N")

        InsertItem("ORC_1_Order_Control", IIf(r.ORC_1_1_Order_Control Is Nothing OrElse r.ORC_1_1_Order_Control = "", "RE", r.ORC_1_1_Order_Control))
        InsertItem("ORC_3_1_Filler_Order_Number_Identifier", r.ORC_3_1_Placer_Order_Number_Identifier)
        InsertItem("ORC_3_2_Filler_Order_Number_Identifier", IIf(r.ORC_3_2_Placer_Order_Number_Namespace Is Nothing OrElse r.ORC_3_2_Placer_Order_Number_Namespace = "", "Respiratory", r.ORC_3_2_Placer_Order_Number_Namespace))

        InsertItem("OBR_1_Sequence_Number", r.OBR_1_Set_ID)
        InsertItem("OBR_3_1_Filler_Order_Number_identifier", r.OBR_3_1_Filler_Order_Number_identifier)
        InsertItem("OBR_3_2_Filler_Order_Number_namespace", IIf(r.OBR_3_2_Filler_Order_Number_namespace Is Nothing OrElse r.OBR_3_2_Filler_Order_Number_namespace = "", "Respiratory", r.OBR_3_2_Filler_Order_Number_namespace))
        InsertItem("OBR_4_1_Universal_Service_Identifier_code", r.OBR_4_1_Universal_Service_Identifier_code)
        InsertItem("OBR_4_2_Universal_Service_Identifier_description", r.OBR_4_2_Universal_Service_Identifier_description)
        'InsertItem("OBR_4_3_Universal_Service_Identifier_type_coding_system", r.OBR_4_3_Universal_Service_Identifier_type_coding_system)
        InsertItem("OBR_7_Observation_Date_Time", r.OBR_7_Observation_Date_Time)
        'InsertItem("OBR_8_Observation_End_Date_Time", r.OBR_8_Observation_End_Date_Time)
        InsertItem("OBR_24_Diagnostic_Serv_Sect_ID", IIf(r.OBR_24_Diagnostic_Serv_Sect_ID Is Nothing OrElse r.OBR_24_Diagnostic_Serv_Sect_ID = "", "GRP", r.OBR_24_Diagnostic_Serv_Sect_ID))
        InsertItem("OBR_25_Result_Status", r.OBR_25_Result_Status)
        'InsertItem("OBR_27_1_Quantity_Timing_quantity", r.OBR_27_1_Quantity_Timing_quantity)
        'InsertItem("OBR_27_3_Quantity_Timing_duration", r.OBR_27_3_Quantity_Timing_duration)
        InsertItem("OBR_27_4_Quantity_Timing_start_datetime", r.OBR_27_4_Quantity_Timing_start_datetime)
        'InsertItem("OBR_27_5_Quantity_Timing_end_datetime", r.OBR_27_5_Quantity_Timing_end_datetime)

        InsertItem("OBX_1_Set_ID", "1")
        InsertItem("OBX_2_Value_Type", r.OBX1_2_Value_Type)
        InsertItem("OBX_3_1_Observation_Identifier_code", r.OBX1_3_1_Observation_Identifier_code)
        InsertItem("OBX_3_2_Observation_Identifier_description", r.OBX1_3_2_Observation_Identifier_description)
        'InsertItem("OBX_3_3_Observation_Identifier_type", r.OBX1_3_3_Observation_Identifier_type)
        InsertItem("OBX_5_Observation_Result", r.OBX1_5_1_Observation_Result)
        'InsertItem("OBX_8_Abnormal_Flags", r.OBX1_8_Abnormal_Flags)
        InsertItem("OBX_11_Observation_Result_Status", r.OBX1_11_Observation_Result_Status)
        InsertItem("OBX_14_Date_Time_of_the_Observation", r.OBX1_14_Date_Time_of_the_Observation)


        Dim OBX As String = Chr(13) + "OBX|||^||^IMAGEURL^IMAGE^IMAGE|||||||||" + Chr(13)      'add extra chr(13) to make the segment recognition logic work

        InsertItem("OBX_1_Set_ID", "2", OBX)
        InsertItem("OBX_2_Value_Type", r.OBX2_2_Value_Type, OBX)
        InsertItem("OBX_3_1_Observation_Identifier_code", r.OBX2_3_1_Observation_Identifier_code, OBX)
        InsertItem("OBX_3_2_Observation_Identifier_description", r.OBX2_3_2_Observation_Identifier_description, OBX)
        'InsertItem("OBX_3_3_Observation_Identifier_type", r.OBX2_3_3_Observation_Identifier_type, OBX)
        InsertItem("OBX_5_Observation_Result", r.OBX2_5_1_Observation_Result, OBX)
        'InsertItem("OBX_8_Abnormal_Flags", r.OBX2_8_Abnormal_Flags, OBX)
        InsertItem("OBX_11_Observation_Result_Status", r.OBX2_11_Observation_Result_Status, OBX)
        InsertItem("OBX_14_Date_Time_of_the_Observation", r.OBX2_14_Date_Time_of_the_Observation, OBX)
        If OBX <> Chr(13) + "GT1|||^^^^||^^^^||||||||||||||||" + Chr(13) Then
            MSG = MSG + OBX.Substring(1)        'remove the extra chr(13) before joing strings back toghether
        End If

    End Sub

    Private Sub InsertItem(ByVal ItemName As String, ByVal value As String, Optional ByRef Message As String = Nothing)
        Dim baseMSG As Boolean = False
        If Message Is Nothing Then
            Message = MSG
            baseMSG = True
        End If
        If ItemName IsNot Nothing AndAlso Not String.IsNullOrEmpty(value) AndAlso ItemName.Contains("_") AndAlso ItemName.Length > 5 Then
            'cut up ItemName to find coordinates
            ItemName = ItemName.Trim
            Dim arr = ItemName.Split("_")
            Dim seg = arr(0).ToString
            Dim pipes = CType(arr(1), Integer)
            Dim HasCarrot As Boolean = False
            Dim carrot As Integer
            If IsNumeric(arr(2)) Then
                carrot = CType(arr(2), Integer) - 1
                HasCarrot = True
            End If

            Dim Location As Integer
            If HasCarrot Then
                Location = loopMSGTo("^", carrot, loopMSGTo("|", pipes, Message.LastIndexOf(Chr(13) + seg + "|"), Message), Message)
            Else
                Location = loopMSGTo("|", pipes, Message.LastIndexOf(Chr(13) + seg + "|"), Message)
            End If

            'validate characters
            value = value.Trim.Replace("|", "").Replace("^", "")
            Message = Message.Insert(Location, value)
            If baseMSG Then
                MSG = Message
            End If
        End If
    End Sub
    Private Function loopMSGTo(ByVal delimeter As Char, ByVal i As Integer, ByVal startIndex As Integer, ByRef Message As String) As Integer
        Dim at As Integer = startIndex
        Try
            For f = 0 To i - 1
                at = Message.IndexOf(delimeter, at)
                If at > 0 Then at += 1
            Next
            Return at
        Catch ex As Exception
            Dim hhh = 7384
        End Try
    End Function

    'Public Sub New(ByVal r As HL7_BillingSender.RftResult, ByVal p As HL7_BillingSender.Demographic, ByVal sf As String)
    '    Dim ReportType As String = "R"
    '    MCID = ReadStep(sf, Logger.stepType.count)
    '    Dim bTemplate As New StringBuilder
    '    MCID += 1
    '    'MSH
    '    bTemplate.Append("MSH|^~\&|RESMED|1031|PBRC|1031|")
    '    bTemplate.Append(Format(Now, "yyyyMMddHHmmss"))
    '    bTemplate.Append("||DFT^P03|DA")
    '    bTemplate.Append(MCID)
    '    bTemplate.Append(Format(Now, "mmss"))
    '    bTemplate.Append("|P|2.4")
    '    bTemplate.Append(Chr(13))

    '    WriteStep(sf, Logger.stepType.count, MCID)

    '    'PID
    '    bTemplate.Append("PID|||")
    '    bTemplate.Append(p.VPatient_Id)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.Surname)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.FirstName)
    '    bTemplate.Append("^^^")
    '    bTemplate.Append(p.Title)
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(p.DOB, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VSex)
    '    bTemplate.Append("||^")
    '    bTemplate.Append(p.Race)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.Address)
    '    bTemplate.Append("^^")
    '    bTemplate.Append(p.Suburb)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.State)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.PostCode)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.VPhone)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VPhoneWork)
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append("|")
    '    bTemplate.Append(Chr(13))

    '    'PV1
    '    bTemplate.Append("PV1||O|")
    '    bTemplate.Append(Chr(13))

    '    'FT1
    '    bTemplate.Append("FT1||")
    '    bTemplate.Append(r.BillingSessionID)
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(r.TestDate, "yyyyMMddHHmmss"))
    '    bTemplate.Append("||")
    '    bTemplate.Append("CG")
    '    bTemplate.Append("|")
    '    bTemplate.Append(r.VBillingItems)
    '    bTemplate.Append("|||||||||||||^")
    '    bTemplate.Append(r.Scientist)
    '    bTemplate.Append("|")
    '    bTemplate.Append(r.RequestingMO_Pn)
    '    bTemplate.Append("||")
    '    bTemplate.Append(r.ResultsID)
    '    bTemplate.Append("|")
    '    bTemplate.Append(r.TypedBy)
    '    bTemplate.Append("|")
    '    bTemplate.Append(ReportType)
    '    bTemplate.Append(Chr(13))

    '    'ZPH
    '    bTemplate.Append("ZPH|||||")
    '    bTemplate.Append(Format(r.RequestDate, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append("")    'where is ref_date???
    '    bTemplate.Append(Chr(13))

    '    MSG = bTemplate.ToString
    'End Sub
    'Public Sub New(ByVal s As HL7_BillingSender.SlpResult, ByVal p As HL7_BillingSender.Demographic, ByVal sf As String)
    '    Dim ReportType As String = "S"
    '    MCID = ReadStep(sf, Logger.stepType.count)
    '    Dim bTemplate As New StringBuilder
    '    MCID += 1
    '    'MSH
    '    bTemplate.Append("MSH|^~\&|RESMED|1031|PBRC|1031|")
    '    bTemplate.Append(Format(Now, "yyyyMMddHHmmss"))
    '    bTemplate.Append("||DFT^P03|DA")
    '    bTemplate.Append(MCID)
    '    bTemplate.Append(Format(Now, "mmss"))
    '    bTemplate.Append("|P|2.4")
    '    bTemplate.Append(Chr(13))

    '    WriteStep(sf, Logger.stepType.count, MCID)

    '    'PID
    '    bTemplate.Append("PID|||")
    '    bTemplate.Append(p.VPatient_Id)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.Surname)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.FirstName)
    '    bTemplate.Append("^^^")
    '    bTemplate.Append(p.Title)
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(p.DOB, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VSex)
    '    bTemplate.Append("||^")
    '    bTemplate.Append(p.Race)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.Address)
    '    bTemplate.Append("^^")
    '    bTemplate.Append(p.Suburb)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.State)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.PostCode)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.VPhone)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VPhoneWork)
    '    bTemplate.Append(Chr(13))

    '    'PV1
    '    bTemplate.Append("PV1||O|")
    '    bTemplate.Append(Chr(13))

    '    'FT1
    '    bTemplate.Append("FT1||")
    '    bTemplate.Append(s.StudyID) 'billingSessionId
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(s.StudyDate, "yyyyMMddHHmmss")) 'testdate
    '    bTemplate.Append("||")
    '    bTemplate.Append("CG")
    '    bTemplate.Append("|")
    '    bTemplate.Append(s.VBillingItems)
    '    bTemplate.Append("|||||||||||||^")
    '    bTemplate.Append(s.StudyPerformedBy)    'scientist
    '    bTemplate.Append("|")
    '    bTemplate.Append(s.RequestingMO_ProviderNum)
    '    bTemplate.Append("||")
    '    bTemplate.Append("")    'resultsId
    '    bTemplate.Append("|")
    '    bTemplate.Append(s.TypedBy)
    '    bTemplate.Append("|")
    '    bTemplate.Append(ReportType)
    '    bTemplate.Append(Chr(13))

    '    'ZPH
    '    bTemplate.Append("ZPH|||||")
    '    bTemplate.Append(Format(s.RequestDate, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append(Format(s.Ref_Date, "yyyyMMddHHmmss"))    'where is ref_date???
    '    bTemplate.Append(Chr(13))

    '    MSG = bTemplate.ToString
    'End Sub

    'Public Sub New(ByVal e As HL7_BillingSender.ExResult, ByVal p As HL7_BillingSender.Demographic, ByVal sf As String)
    '    Dim ReportType As String = "E"
    '    MCID = ReadStep(sf, Logger.stepType.count)
    '    Dim bTemplate As New StringBuilder
    '    MCID += 1
    '    'MSH
    '    bTemplate.Append("MSH|^~\&|RESMED|1031|PBRC|1031|")
    '    bTemplate.Append(Format(Now, "yyyyMMddHHmmss"))
    '    bTemplate.Append("||DFT^P03|DA")
    '    bTemplate.Append(MCID)
    '    bTemplate.Append(Format(Now, "mmss"))
    '    bTemplate.Append("|P|2.4")
    '    bTemplate.Append(Chr(13))

    '    WriteStep(sf, Logger.stepType.count, MCID)

    '    'PID
    '    bTemplate.Append("PID|||")
    '    bTemplate.Append(p.VPatient_Id)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.Surname)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.FirstName)
    '    bTemplate.Append("^^^")
    '    bTemplate.Append(p.Title)
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(p.DOB, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VSex)
    '    bTemplate.Append("||^")
    '    bTemplate.Append(p.Race)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.Address)
    '    bTemplate.Append("^^")
    '    bTemplate.Append(p.Suburb)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.State)
    '    bTemplate.Append("^")
    '    bTemplate.Append(p.PostCode)
    '    bTemplate.Append("||")
    '    bTemplate.Append(p.VPhone)
    '    bTemplate.Append("|")
    '    bTemplate.Append(p.VPhoneWork)
    '    bTemplate.Append(Chr(13))

    '    'PV1
    '    bTemplate.Append("PV1||O|")
    '    bTemplate.Append(Chr(13))

    '    'FT1
    '    bTemplate.Append("FT1||")
    '    bTemplate.Append(e.BillingSessionID)
    '    bTemplate.Append("||")
    '    bTemplate.Append(Format(e.TestDate, "yyyyMMddHHmmss"))
    '    bTemplate.Append("||")
    '    bTemplate.Append("CG")
    '    bTemplate.Append("|")
    '    bTemplate.Append(e.VBillingItems)
    '    bTemplate.Append("|||||||||||||^")
    '    bTemplate.Append(e.Scientist)
    '    bTemplate.Append("|")
    '    bTemplate.Append(e.RequestingMO_Pn)
    '    bTemplate.Append("||")
    '    bTemplate.Append(e.ResultsID)
    '    bTemplate.Append("|")
    '    bTemplate.Append(e.Typist)
    '    bTemplate.Append("|")
    '    bTemplate.Append(ReportType)
    '    bTemplate.Append(Chr(13))

    '    'ZPH
    '    bTemplate.Append("ZPH|||||")
    '    bTemplate.Append(Format(e.RequestDate, "yyyyMMddHHmmss"))
    '    bTemplate.Append("|")
    '    bTemplate.Append("")    'where is ref_date???
    '    bTemplate.Append(Chr(13))

    '    MSG = bTemplate.ToString
    'End Sub
End Class
